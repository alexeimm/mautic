<?php
// plugins/MFPlugin/ContactHash.php

namespace MauticPlugin\ContactHash;

use Mautic\PluginBundle\Bundle\PluginBundleBase;

use Doctrine\DBAL\Schema\Schema;

use Mautic\PluginBundle\Entity\Plugin;
use Mautic\CoreBundle\Factory\MauticFactory;




class ContactHash extends PluginBundleBase
{
    
    static public function onPluginInstall(Plugin $plugin, MauticFactory $factory, $metadata = null)
    {
        if ($metadata !== null) {
            self::installPluginSchema($metadata, $factory);
        }
    
    }

}

