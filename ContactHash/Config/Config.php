<?php
/**
 * @copyright   2020 datafor.xyz. All rights reserved
 * @author      datafor.xyz, Inc
 * @link https://datafor.xyz/
 */

return array(
    //simplificando todo; tengo ciertas dudas, pero, dado lo que sé considero que la config del plugin (config.php) consta de 3 partes
    #1 información del plugin y author
    'name'        => 'ContactHash',
    'description' => 'A Mautic plugin to automatically update add a hash to a contact based on their email',
    'version'     => '1.0',
    'author'      => 'Datafor.xyz',
    
    #2 ruta del controlador del plugin https://developer.mautic.org/#controllers
    'routes'   => array(
        'main' => array(
            'mautic_contacthash' => array(
                'path'       => '/emails/{page}', //está ruta la estoy asumiento por los momentos
                'controller' => 'ContactHash:Default:admin', // :reloadAction
                'defaults'    => array(
                    'Assets' => 'img/contacthash.png'),
                                ),    
            )
        ),
    
    #3 ruta del evento 
    'services' => array(
        'events' => array(
            'plugin.contacthash.emailbundle.subscriber' => array(
                'class'     => 'MauticPlugin\ContactHash\EventListener\EmailSubscriber')
        ),        
      )               
      
);    
      
        
    
    
  
    
    
    
    
    