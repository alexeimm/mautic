<?php

/*
 * @copyright   2020 datafor.xyz. All rights reserved
 * @author      datafor.xyz, Inc
 */

namespace MauticPlugin\ContactHash\EventListener;  

use Mautic\CoreBundle\EventListener\CommonSubscriber; // https://developer.mautic.org/#events
use Mautic\EmailBundle\EmailEvents; // EMAIL_ON_BUILD y otros eventos necesarios
use Mautic\EmailBundle\Event\EmailBuilderEvent;  // de aqui \CoreBundle\Event\BuilderEvent; llama esa funcion para addtokensection
use Mautic\EmailBundle\Event\EmailSendEvent;

/*
 Class EmailSubscriber. lo que significa que a la clase EmailSubscriber le extendemos la function EventSubscriberInterface
*/

class EmailSubscriber extends CommonSubscriber // tecnicamente "class StoreSubscriber implements EventSubscriberInterface"
{ //viene de aqui https://symfony.com/doc/current/components/event_dispatcher.html#using-event-subscribers
     /** it is used to tell the script what kind of datatype 
      * the variables are and what kind of value a function returns;
     * @return array
     */
    public static function getSubscribedEvents()
    {   
        return array(
            EmailEvents::EMAIL_ON_BUILD   => array('onEmailBuild', 0),
            EmailEvents::EMAIL_ON_SEND    => array('onEmailGenerate', 0),
            EmailEvents::EMAIL_ON_DISPLAY => array('onEmailGenerate', 0)
                    ); //corebundle/eventlistener/commonsubscribed.php
    }//

    /** 
    * @param EmailBuilderEvent $event
     */
    public function onEmailBuild(EmailBuilderEvent $event)
    { //https://developer.mautic.org/#views &  https://developer.mautic.org/#extending-emails
        $content = $this->templating->render('ContactHash:SubscribedEvents\EmailToken:token.html.php');
        
        $event->addTokenSection('contacthash.token', 'plugin.contacthash.builder.header', $content); // Mautic\CoreBundle\Event\BuilderEvent;
        /*  $event->addTokenSection($uniqueId, $headerTranslationKey, $htmlContent)
         is used to generate the section for drag and drop tokens in the email builder events	Defines event subscriber classes used to listen to events 
            dispatched throughout Mautic and auto-tagged with 
            'kernel.event_subscriber.’    https://developer.mautic.org/?php#events
        $uniqueId must be unique.
        $headerTranslationKey is the translation key that will be used to create the section’s header.
        $htmlContent is the HTML that will be inserted into the builder’s token list for this token’s section.
         $this->templating->render() can be used to render a specific view’s content (using view notation.
        There is free reign as to what the HTML will look like but the important part is that the elements
        representing the tokens must have the attribute data-token="{token_text}" in order for the builder to recognize them.

        For example, <a href="#" data-token="{hello}" class="btn btn-default btn-block">Translated Token Text</a>
        */
    } 

        /**
     * Search and replace tokens with content
     *
     * @param EmailSendEvent $event
     */     
    public function onEmailGenerate(EmailSendEvent $event)
    {
 
        $content = $event->getContent();
        
        $RandomToken = uniqid();
        
        $content = str_replace('{contacthash.token}', $RandomToken, $content);
       
        $event->setContent($content);

    }

}




    